import { Component, ViewChild } from '@angular/core';
import { Camera, NativeStorage } from 'ionic-native';
import { NavController, LoadingController, AlertController, ModalController, MenuController } from 'ionic-angular';
import { FormBuilder, Validators, FormControl, FormControlName } from '@angular/forms';
import { AuthData } from '../../providers/auth-data';
import { EmailValidator } from '../../validators/email';


import { TermsConditionsPage } from '../terms-conditions/terms-conditions';
import { HomePage } from '../home/home';
import { PersonalInformationPage } from '../personal-information/personal-information';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})
export class SignUpPage {

  public signupForm;
  public disabled: boolean = true;

  emailChanged: boolean = false;

  passwordChanged: boolean = false;

  submitAttempt: boolean = false;
  photoRef: any;
  loading: any;

  constructor(public navCtrl: NavController, public authData: AuthData,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public menuCtrl: MenuController) {

    this.menuCtrl.enable(false);
    this.signupForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }
  /**
  * Receives an input field and sets the corresponding fieldChanged property to 'true' to help with the styles.
  */
  elementChanged(input) {
    let field = input.inputControl.name;
    this[field + "Changed"] = true;
  }

  /*signupUser() {
    this.submitAttempt = true;

    if (!this.signupForm.valid) {
      // this.signupSlider.slideTo(0);
      console.log(this.signupForm.value);
    } else {
      this.authData.signUpUser(
        // this.signupForm.value.fullName,
        this.signupForm.value.firstName,
        this.signupForm.value.lastName,
        this.signupForm.value.email,
        // this.signupForm.value.address.streetAddress,
        this.signupForm.value.address.city,
        this.signupForm.value.address.state,
        this.signupForm.value.address.zipcode,
        this.signupForm.value.phoneNumber,
        this.signupForm.value.ssn,
        this.signupForm.value.drivingCredentials.drivingLicense,
        this.signupForm.value.drivingCredentials.expirationDate,
        this.signupForm.value.carDetails.carMake,
        this.signupForm.value.carDetails.carModel,
        this.signupForm.value.carDetails.carYear,
        this.signupForm.value.carDetails.carColor,
        this.signupForm.value.driverAccountDetails.bankName,
        this.signupForm.value.driverAccountDetails.accountHolderName,
        this.signupForm.value.driverAccountDetails.accountNumber,
        this.signupForm.value.driverAccountDetails.abaNumber,
        this.signupForm.value.password,
        this.photoRef, this.carRegPhotoRef, this.dmvPhotoRef
      ).then((response) => {
        console.log('signup completed');
        console.log(response);
        this.navCtrl.setRoot(HomePage);
      }, (error) => {
        this.loading.dismiss().then(() => {
          var errorMessage: string = error.message;
          let alert = this.alertCtrl.create({
            message: errorMessage,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }*/

  driverPicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.photoRef = imageData;
      this.disabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  driverPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.photoRef = imageData;
      this.disabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  enable(e) {
    console.log(e);
    if (e.checked === true) {
      this.disabled = false;
    }
    else {
      this.disabled = true;
    }
  }


  continue() {
    this.submitAttempt = true;
    if (!this.signupForm.valid) {
      console.log('something went wrong')
    } else {
      let loading = this.loadingCtrl.create({
        content: 'please wait...',
        dismissOnPageChange: true
      });
      loading.present();

      console.log(this.signupForm.value);
      NativeStorage.setItem('user',
        {
          email: this.signupForm.value.email,
          pass: this.signupForm.value.password,
          // picture: user.picture
        })
        .catch((err) => { console.log(err) });

      this.authData.createUser(this.signupForm.value.email, this.signupForm.value.password, this.photoRef)
        .then(res => {
          console.log(res);
          loading.dismiss();
          // this.navCtrl.push(PersonalInformationPage);
          this.navCtrl.setRoot(PersonalInformationPage);
        }, (error) => {
          loading.dismiss().then(() => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        })
    }
  }

  ionViewDidLoad() {
    console.log('Hello SignUpPage Page');
  }

}
