import { Component } from '@angular/core';
import { NavController, AlertController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-extra-cost',
  templateUrl: 'extra-cost.html'
})
export class ExtraCostPage {

  description: string = "Description";
  cost: number = 0;
  extraCost: boolean = false;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('Hello ExtraCostPage Page');
  }
  addCost() {
    if(this.description === "Description" || this.cost === 0) {
      let alert = this.alertCtrl.create({
        title: "Please enter cost and description",
        buttons: ["Ok"]
      });
      alert.present();
    } else {
      let data = {description: this.description, cost: this.cost};
      this.viewCtrl.dismiss(data);
    }
  }

  confirmCost() {
    this.extraCost = true;
    let data = {cost: this.extraCost};
    this.viewCtrl.dismiss(data);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
