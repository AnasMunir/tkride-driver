import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-end-ride-modal',
  templateUrl: 'end-ride-modal.html'
})
export class EndRideModalPage {
  distance: string;
  duration: string;
  points: string;
  extraCost: boolean = false;
  totalFair: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController) {
      this.distance = navParams.get('distance');
      this.duration = navParams.get('duration');
      this.totalFair = navParams.get('totalFair')
      // this.points = this.distance.replace("mi", "points");
  }

  ionViewDidLoad() {
    console.log('Hello EndRideModalPage Page');
  }

  dismiss() {
    this.viewCtrl.dismiss(this.extraCost);
  }

  enableCost(e) {
    console.log(e);
    if (e.checked === true) {
      this.extraCost = true;
    }
    else {
      this.extraCost = false;
    }
  }
}
