import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController } from 'ionic-angular';
import { Camera, NativeStorage } from 'ionic-native';
import { FormBuilder, Validators, FormControl, FormControlName, NgControl } from '@angular/forms';

import { TermsConditionsPage } from '../terms-conditions/terms-conditions';
import { InsuranceInformationPage } from '../insurance-information/insurance-information'

@Component({
  selector: 'page-car-information',
  templateUrl: 'car-information.html'
})
export class CarInformationPage {

  public signupCarForm: any;
  public disabled = true;
  submitAttempt: boolean = false;
  photoRef: any; carRegPhotoRef: any; dmvPhotoRef: any;
  photoDisabled: boolean = true;
  regDisabled: boolean = true;
  dmvDisabled: boolean = true;

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public formBuilder: FormBuilder) {

    this.signupCarForm = formBuilder.group({

      drivingLicense: ['', Validators.required],
      expirationDate: ['', Validators.required],
      licensePlateNumber: ['', Validators.compose([Validators.maxLength(10), Validators.required])],
      carMake: ['', Validators.required],
      carBrand: ['', Validators.required],
      carYear: ['', Validators.required],
      carColor: ['', Validators.required],
    })
  }

  ionViewDidLoad() {
    console.log('Hello CarInformationPage Page');
  }

  takePicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.photoRef = imageData;
      this.photoDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takePictureFromPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.photoRef = imageData;
      this.photoDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  carRegistrationPicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.carRegPhotoRef = imageData;
      this.regDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  carRegistrationPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.carRegPhotoRef = imageData;
      this.regDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  dmvPicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.dmvPhotoRef = imageData;
      this.dmvDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  dmvPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.dmvPhotoRef = imageData;
      this.dmvDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  enable(e) {
    console.log(e);
    if (e.checked === true) {
      this.disabled = false;
    }
    else {
      this.disabled = true;
    }
  }

  // termsAndConditions() {
  //   let modal = this.modalCtrl.create(TermsConditionsPage);
  //   modal.present();
  // }

  continue() {
    this.submitAttempt = true;
    if (!this.signupCarForm.valid) {
      console.log('something went wrong')
    } else {
      // console.log(this.signupCarForm.value)
      NativeStorage.setItem('carInfo', {
        drivingLicense: this.signupCarForm.value.drivingLicense,
        expirationDate: this.signupCarForm.value.expirationDate,
        licensePlateNumber: this.signupCarForm.value.licensePlateNumber,
        carMake: this.signupCarForm.value.carMake,
        carBrand: this.signupCarForm.value.carBrand,
        carYear: this.signupCarForm.value.carYear,
        carColor: this.signupCarForm.value.carColor,
        photoRef: this.photoRef,
        carRegPhotoRef: this.carRegPhotoRef,
        dmvPhotoRef: this.dmvPhotoRef
      })
      this.navCtrl.push(InsuranceInformationPage);
    }
  }

}
