import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Camera, NativeStorage } from 'ionic-native';
import { FormBuilder, Validators, FormControl, FormControlName } from '@angular/forms';

import { LegalInformationPage } from '../legal-information/legal-information';

@Component({
  selector: 'page-insurance-information',
  templateUrl: 'insurance-information.html'
})
export class InsuranceInformationPage {

  public signupInsuranceForm: any;
  submitAttempt: boolean = false;
  insuranceCardPicture: any; insuranceCardPhoto: any;
  insursanceDisabled: boolean = true;

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder) {

    this.signupInsuranceForm = formBuilder.group({
      insuranceName: ['', Validators.required],
      policyNumber: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('Hello InsuranceInformationPage Page');
    NativeStorage.getItem('carInfo').then(data => {
      console.log(data);
    }).catch(err => { console.log(err) });
  }

  takePicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.insuranceCardPhoto = imageData;
      this.insursanceDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takePictureFromPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      this.insuranceCardPhoto = imageData;
      this.insursanceDisabled = false;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  continue() {
    this.submitAttempt = true;
    if (!this.signupInsuranceForm.valid) {
      console.log('something went wrong')
    } else {
      NativeStorage.setItem('insuranceInfo', {
        insuranceName: this.signupInsuranceForm.value.insuranceName,
        policyNumber: this.signupInsuranceForm.value.policyNumber,
        insuranceCardPhoto: this.insuranceCardPhoto
      })
      this.navCtrl.push(LegalInformationPage);
    }
  }

}
