import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { EditProfilePage } from '../edit-profile/edit-profile';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  fullname: any; email: any; city: any; state: any; zipcode: any; phonenumber: any;
  ssn: any; bankName: any; accountHolderName: any; abaNumber: any; accountNumber: any;
  drivingLicense: any; expirationDate: any; licensePlateNumber: any;
  carMake: any; carBrand: any; carColor: any; carYear: any;
  insuranceName: any; policyNumber: any; tcpNumber: any;
  driveForUber: any; driveForLyft: any; driveForLimo: any; ownLimo: any;
  profilePhoto: any;

  carPicture: any; insuranceCardPhoto: any; dmvPicture: any; carRegistrationPicture: any;
  constructor(public navCtrl: NavController) {
    let ssn = this.ssn;
    console.log(ssn);
    // this.ssn = new Array(6).join('x')
  }

  ionViewDidLoad() {
    console.log('Hello ProfilePage Page');
    let driver = firebase.auth().currentUser;
    let ref = firebase.database().ref('drivers/'+driver.uid);
    ref.once("value", (snap) => {
      this.profilePhoto = snap.val().driverPicture;
      this.carPicture = snap.val().carPicture;
      this.dmvPicture = snap.val().dmvPicture;
      this.carRegistrationPicture = snap.val().carRegistrationPicture;
      this.insuranceCardPhoto = snap.val().insuranceCardPhoto;

      this.fullname = snap.val().fullname;
      this.email = snap.val().email;
      this.city = snap.val().city;
      this.state = snap.val().state;
      this.zipcode = snap.val().zipcode;
      this.phonenumber = snap.val().phonenumber;

      this.ssn = snap.val().ssn;
      this.bankName = snap.val().bankName;
      this.accountHolderName = snap.val().accountHolderName;
      this.abaNumber = snap.val().abaNumber;
      this.accountNumber = snap.val().accountNumber;

      this.drivingLicense = snap.val().drivingLicense;
      this.expirationDate = snap.val().expirationDate;
      this.licensePlateNumber = snap.val().licensePlateNumber;

      this.carMake = snap.val().carMake;
      this.carBrand = snap.val().carBrand;
      this.carColor = snap.val().carColor;
      this.carYear = snap.val().carYear;

      this.insuranceName = snap.val().insuranceName;
      this.policyNumber = snap.val().policyNumber;
      this.tcpNumber = snap.val().tcpNumber;

      this.driveForUber = snap.val().driveForUber;
      this.driveForLyft = snap.val().driveForLyft;
      this.driveForLimo = snap.val().driveForLimo;
      this.ownLimo = snap.val().ownLimo;


    });
  }
  editProfile() {
    this.navCtrl.push(EditProfilePage,
      {
        fullName: this.fullname,
        city: this.city,
        state: this.state,
        zipcode: this.zipcode,
        phoneNumber: this.phonenumber,
        ssn: this.ssn,
        bankName: this.bankName,
        accountHolderName: this.accountHolderName,
        abaNumber: this.abaNumber,
        drivingLicense: this.drivingLicense,
        expirationDate: this.expirationDate,
        licensePlateNumber: this.licensePlateNumber,
        carMake: this.carMake,
        carBrand: this.carBrand,
        carColor: this.carColor,
        carYear: this.carYear,
        insuranceName: this.insuranceName,
        policyNumber: this.policyNumber,
        driveForUber: this.driveForUber,
        driveForLyft: this.driveForLyft,
        driveForLimo: this.driveForLimo,
        ownLimo: this.ownLimo,
        carPicture: this.carPicture,
        dmvPicture: this.dmvPicture,
        carRegistrationPicture: this.carRegistrationPicture,
        insuranceCardPhoto: this.insuranceCardPhoto,
        profilePhoto: this.profilePhoto
      });
  }

}
