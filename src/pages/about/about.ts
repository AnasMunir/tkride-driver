import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

declare var cordova: any;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello AboutPage Page');
  }
  openBrowser() {
    cordova.InAppBrowser.open('http://www.tkride.com/', '_blank', 'location=yes');
  }

}
