import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SocialSharing } from 'ionic-native';

@Component({
  selector: 'page-support',
  templateUrl: 'support.html'
})
export class SupportPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello SupportPage Page');
  }

  openEmail() {
     SocialSharing.shareViaEmail('', '', ['support@butler247.com']).then(() => {
      console.log('opened email');
    }).catch((err) => {
      console.log('an error happend');
      console.log(err);
    });
  }
}
