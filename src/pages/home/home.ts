import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ModalController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { GoogleMap, GoogleMapsEvent, GoogleMapsLatLng, Geolocation, Geoposition, BackgroundGeolocation } from 'ionic-native';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

// importing providers
import { GoogleMaps } from '../../providers/google-maps';
import { AuthData } from '../../providers/auth-data';
import { FcmNotification } from '../../providers/fcm-notification'

// importing pages
import { RequestModalPage } from '../request-modal/request-modal';
import { RidePage } from '../ride/ride';

declare var FCMPlugin: any;
declare var google: any;
declare var GeoFire: any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  @ViewChild('pleaseTurnOnLocation') pleaseTurnOnLocation: ElementRef;

  public lat: number = 31.5131486;
  public lng: number = 74.3153909;
  public driverToken; public driverName; public carMake;

  ionViewDidLoad() {
    console.log('Hello Driver HomePage');
  }
  ngOnInit() {
    let driver = firebase.auth().currentUser;
    let ref = firebase.database().ref('drivers/' + driver.uid);
    FCMPlugin.getToken(
      (token) => {
        // alert(token);
        console.log(token);
        ref.update({ fcm_token: token });
      },
      (err) => {
        console.log('error retrieving token: ' + err);
      }
    );
  }
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public authData: AuthData,
    public fcmNotification: FcmNotification,
    public maps: GoogleMaps,
    public platform: Platform, public menu: MenuController) {

    menu.enable(true);

    /*var presenceRef = firebase.database().ref();
    
    presenceRef.onDisconnect().set("I disconnected!");*/

    let self = this;

    let alert = this.alertCtrl.create({
      title: 'Location disabled!',
      subTitle: 'please turn on your location',
      buttons: [{
        text: 'OK',
        /*handler: () => {
          this.navCtrl.setRoot(HomePage);
        }*/
      }]
    });
    alert.onDidDismiss(_ => {
      this.navCtrl.setRoot(HomePage);
    })
    this.platform.ready().then(() => {
      Geolocation.getCurrentPosition().then((pos: Geoposition) => {
        console.log(pos);
      }).then(() => {
        let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);
      }, error => {
        // alert.present();
        console.log(error);
      }).catch(err => {
        console.log('setlocation error geolocation');
        // console.log(this.pleaseConnect.nativeElement.querySelector('p'));
        // this.pleaseConnect.nativeElement.style.display = "block";
        console.log(err);
      })
    });

    setInterval(this.setLocations, 5000);

    let driver = firebase.auth().currentUser;
    let ref = firebase.database().ref('drivers/' + driver.uid);

    ref.once("value", (snap) => {
      this.driverToken = snap.val().fcm_token;
      this.driverName = snap.val().fullname
      this.carMake = snap.val().car_make;
      console.log('driver token: ' + this.driverToken);
      // self.fcmNotification.sendNotificaiton(driver_token, clientToken, clientName, this.from, this.to);
    });

    FCMPlugin.onNotification(
      (data) => {
        if (data.wasTapped) {
          console.log(data);

          let confirm = this.modalCtrl.create(RequestModalPage,
            {
              clientName: data.clientName,
              pickupLocation: data.pickupLocation,
              dropoffLocation: data.dropoffLocation,
              lat: data.lat,
              lng: data.lng,
              clientToken: data.From,
              userUid: data.userUid
            });
          confirm.onDidDismiss((data) => {
            console.log('Dismiss request modal');
            console.log(data);
            if (data) {
              this.navCtrl.setRoot(RidePage, { clientToken: data.clientToken, uid: data.uid, pickupLat: data.pickupLat, pickupLng: data.pickupLng, userUid: data.userUid })
            }
          })
          // console.log('data from ' + data.From)

          confirm.present();


        } else {
          console.log(data);
          let confirm = self.modalCtrl.create(RequestModalPage,
            {
              clientName: data.clientName,
              pickupLocation: data.pickupLocation,
              dropoffLocation: data.dropoffLocation,
              lat: data.lat,
              lng: data.lng,
              clientToken: data.From,
              userUid: data.userUid
            });
          confirm.onDidDismiss((data) => {
            console.log('Dismiss request modal');
            console.log(data);
            if (data) {
              this.navCtrl.setRoot(RidePage, { clientToken: data.clientToken, uid: data.uid, pickupLat: data.pickupLat, pickupLng: data.pickupLng, userUid: data.userUid })
            }
          });
          confirm.present()

        }
      },
      (msg) => {
        console.log('onNotification callback successfully registered: ' + msg);
      },
      (err) => {
        console.log('Error registering onNotification callback: ' + err);
      }
    );

  }
  setLocations() {
    let self = this;



    let user = firebase.auth().currentUser;
    let ref = firebase.database().ref('drivers/locations');

    var geoFire = new GeoFire(ref);
    let options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    Geolocation.getCurrentPosition(options).then((pos: Geoposition) => {

      this.lat = pos.coords.latitude; this.lng = pos.coords.longitude;
      geoFire.set(user.uid, [pos.coords.latitude, pos.coords.longitude]).then(() => {
        ref.child(user.uid).onDisconnect().remove();
      }).catch(err => {
        console.log('setlocation error geofire');
        console.log(err)
      });
    }).catch(err => {
      console.log('setlocation error geolocation');
      /*console.log(this.pleaseConnect.nativeElement.querySelector('p'));
      this.pleaseConnect.nativeElement.style.display = "block";*/
      console.log(err);
    })
  }
  adjustLocation() {
    this.maps.adjustLocation();
    // this.pleaseConnect.nativeElement.style.display = "block";
  }
  ngAfterViewInit() {

    // let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);
    /*Geolocation.getCurrentPosition().then((pos: Geoposition) => {
      console.log(pos);
    }).then(() => {
    }, error => {
      // alert.present();
      console.log(error);
    }).catch(err => {
      console.log('setlocation error geolocation');
      // console.log(this.pleaseConnect.nativeElement.querySelector('p'));
      // this.pleaseConnect.nativeElement.style.display = "block";
      console.log(err);
    });*/
    // setInterval(this.setLocations, 5000);
    /*Geolocation.watchPosition(options).subscribe(pos => {
      console.log(pos);
      if (PositionError) {
        this.pleaseConnect.nativeElement.style.display = "block";
      } else {
        this.pleaseConnect.nativeElement.style.display = "none";
      }
      // console.log(this.pleaseConnect.nativeElement.querySelector('p'));
    }*//*, error => {
      console.log('error in watch postion');
      console.log(this.pleaseConnect.nativeElement.querySelector('p'));
      console.log(error);
    }*/
  }

  locationDisabled() {
    console.log('error from setlocations: ');
    // this.pleaseTurnOnLocation.nativeElement.style.display = "block";
    this.pleaseConnect.nativeElement.style.display = "block";
  }
}
