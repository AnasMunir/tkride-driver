import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { NavController, Platform, NavParams, ViewController } from 'ionic-angular';

// import { RidePage } from '../ride/ride';
// importing providers
import { GoogleMaps } from '../../providers/google-maps';
import { FcmNotification } from '../../providers/fcm-notification';

declare var google: any;

@Component({
  selector: 'page-request-modal',
  templateUrl: 'request-modal.html'
})
export class RequestModalPage /*implements OnInit*/ {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;

  public clientName;
  public userUid;
  public pickupLocation;
  public dropoffLocation
  public lat; public lng;
  public clientToken;
  public timer; public ref;
  // public map: any;

  ionViewDidLoad() {
    console.log('Hello RequestModalPage Page');
    this.platform.ready().then(() => {
      this.maps.requestModalMap(this.mapElement.nativeElement,
        this.pickupLocation, this.dropoffLocation, this.lat, this.lng).catch(err => (console.log(err)));
      this.timer = setTimeout(() => this.timeOut(), 20000);
    }).then(() => {
    }).catch( err => console.log(err));
  }

  // ngOnInit() {

  // }

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public maps: GoogleMaps,
    public platform: Platform,
    public fcmNotification: FcmNotification) {

    this.clientName = this.navParams.get('clientName');
    this.pickupLocation = this.navParams.get('pickupLocation');
    this.dropoffLocation = this.navParams.get('dropoffLocation');
    this.userUid = this.navParams.get('userUid');
    this.lat = this.navParams.get('lat');
    this.lng = this.navParams.get('lng');
    this.clientToken = this.navParams.get('clientToken');
    console.log('client token: ' + this.clientToken);

    this.ref = firebase.auth().currentUser;
    /*let latLng = new google.maps.LatLng(40.713744, -74.009056);
    let mapOptions = {
      center: latLng,
      scroll: true,
      rotate: true,
      zoomControl: false,
      disableDefaultUI: true,
      mapTypeControl: false,
      zoom: 15,
      tilt: 30,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }*/

    // this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  accept() {
    clearTimeout(this.timer);
    this.fcmNotification.sendNotificaiton(this.clientToken, this.ref.uid, this.lat, this.lng, true);
    let data = {'clientToken': this.clientToken, 'uid': this.ref.uid, 'pickupLat': this.lat, 'pickupLng': this.lng, 'userUid': this.userUid};
    this.viewCtrl.dismiss(data);
    // this.navCtrl.setRoot(RidePage, {clientToken: this.clientToken, uid: this.ref.uid ,pickupLat: this.lat, pickupLng: this.lng});
  }
  reject() {
    clearTimeout(this.timer);
    this.fcmNotification.sendNotificaiton(this.clientToken, this.ref.uid, this.lat, this.lng, false);
    this.viewCtrl.dismiss();
  }
  timeOut() {
    clearTimeout(this.timer);
    this.fcmNotification.sendNotificaiton(this.clientToken, this.ref.uid, this.lat, this.lng, false);
    this.viewCtrl.dismiss();
  }

}
