import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormControl, FormControlName } from '@angular/forms';
import { NativeStorage } from 'ionic-native';

import { TermsConditionsPage } from '../terms-conditions/terms-conditions';
import { HomePage } from '../home/home';
import { AuthData } from '../../providers/auth-data';

@Component({
  selector: 'page-legal-information',
  templateUrl: 'legal-information.html'
})
export class LegalInformationPage {

  public signupLegalForm: any;
  submitAttempt: boolean = false;
  public disabled: boolean = true;
  public checkDisabled: boolean = true;
  user: any; email: any; password: any;
  personalInfo: any; firstName: any; lastName: any; city: any; state: any; zipcode: any; phoneNumber: any; ssn: any;
  bankInfo: any; bankName: any; accountHolderName: any; accountNumber: any; abaNumber: any;
  carInfo: any; drivingLicense: any; expirationDate: any; licensePlateNumber: any; carMake: any; carBrand: any; carYear: any; carColor: any; photoRef: any; carRegPhotoRef: any; dmvPhotoRef: any;
  insuranceInfo: any; insuranceName: any; policyNumber: any; insuranceCardPhoto: any;
  legalInfo: any; tcpNumber: any; driveForUber: any; driveForLyft: any; driveForLimo: any; ownLimo: any;
  loading: any;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public authData: AuthData) {

    let modal = this.modalCtrl.create(TermsConditionsPage);
    modal.present();

    this.signupLegalForm = formBuilder.group({
      doYouHaveTcpNumber: ['', Validators.required],
      tcpNumber: [''],
      driveForUber: ['', Validators.required],
      driveForLyft: ['', Validators.required],
      driveForLimo: ['', Validators.required],
      ownLimo: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('Hello LegalInformationPage Page');
    NativeStorage.getItem('user').then(data => {
      this.user = data
      this.email = data.email;
      this.password = data.pass;
    }).catch(err => console.log);
    NativeStorage.getItem('personalInfo').then(data => {
      this.personalInfo = data;
      this.firstName = data.firstName;
      this.lastName = data.lastName;
      this.city = data.city;
      this.state = data.state;
      this.zipcode = data.zipcode;
      this.phoneNumber = data.phoneNumber;
      this.ssn = data.ssn;
    }).catch(err => console.log);
    NativeStorage.getItem('bankInfo').then(data => {
      this.bankInfo = data;
      this.bankName = data.bankName;
      this.accountHolderName = data.accountHolderName;
      this.accountNumber = data.accountNumber;
      this.abaNumber = data.abaNumber;
    }).catch(err => console.log);
    NativeStorage.getItem('carInfo').then(data => {
      this.carInfo = data;
      this.drivingLicense = data.drivingLicense;
      this.expirationDate = data.expirationDate;
      this.licensePlateNumber = data.licensePlateNumber;
      this.carMake = data.carMake;
      this.carBrand = data.carBrand;
      this.carYear = data.carYear;
      this.carColor = data.carColor;
      this.photoRef = data.photoRef;
      this.carRegPhotoRef = data.carRegPhotoRef;
      this.dmvPhotoRef = data.dmvPhotoRef;
    }).catch(err => console.log);
    NativeStorage.getItem('insuranceInfo').then(data => {
      this.insuranceInfo = data;
      this.insuranceName = data.insuranceName;
      this.policyNumber = data.policyNumber;
      this.insuranceCardPhoto = data.insuranceCardPhoto;
    }).catch(err => console.log);
    NativeStorage.getItem('legalInfo').then(data => {
      this.legalInfo = data;
      this.tcpNumber = data.tcpNumber;
      this.driveForUber = data.driveForUber;
      this.driveForLyft = data.driveForLyft;
      this.driveForLimo = data.driveForLimo;
      this.ownLimo = data.ownLimo;
    }).catch(err => console.log);
  }

  enable(e) {
    console.log(e);
    if (e === 'no') {
      this.disabled = true;
    }
    else {
      this.disabled = false;
    }
  }

  enableCheck(e) {
    console.log(e);
    if (e.checked === true) {
      this.checkDisabled = false;
    }
    else {
      this.checkDisabled = true;
    }
  }

  termsAndConditions() {
    let modal = this.modalCtrl.create(TermsConditionsPage);
    modal.present();
  }
  submit() {
    this.submitAttempt = true;
    if (!this.signupLegalForm.valid) {
      console.log('something went wrong')
    } else {
      NativeStorage.setItem('legalInof', {
        tcpNumber: this.signupLegalForm.value.tcpNumber,
        driveForUber: this.signupLegalForm.value.driveForUber,
        driveForLyft: this.signupLegalForm.value.driveForLyft,
        driveForLimo: this.signupLegalForm.value.driveForLimo,
        ownLimo: this.signupLegalForm.value.ownLimo
      })
      console.log(this.signupLegalForm.value);

      this.authData.signUpUser(
        // this.email,
        this.password,
        this.firstName,
        this.lastName,
        // newStreetAddress,
        this.city,
        this.state,
        this.zipcode,
        this.phoneNumber,
        this.ssn,
        this.bankName,
        this.accountNumber,
        this.abaNumber,
        this.accountHolderName,
        this.drivingLicense,
        this.expirationDate,
        this.licensePlateNumber,
        this.carMake,
        this.carBrand,
        this.carYear,
        this.carColor,
        this.photoRef,
        this.carRegPhotoRef,
        this.dmvPhotoRef,
        this.insuranceInfo,
        this.insuranceName,
        this.policyNumber,
        this.insuranceCardPhoto,
        this.signupLegalForm.value.tcpNumber,
        this.signupLegalForm.value.driveForUber,
        this.signupLegalForm.value.driveForLyft,
        this.signupLegalForm.value.driveForLimo,
        this.signupLegalForm.value.ownLimo)
        .then((res) => {
          console.log(res);
          console.log('signup successfull from leagal page');
          // console.timeEnd('lading ended');
          // this.navCtrl.push(WelcomePage)
          let alert = this.alertCtrl.create({
            message: 'Welcome, you have successfully registered with TKRIDE',
            buttons: [
              {
                text: "Ok"
              }
            ]
          });
          alert.present();
          alert.onDidDismiss(_ => {
            this.navCtrl.setRoot(HomePage);
          })
        }, (error) => {
          this.loading.dismiss().then(() => {
            var errorMessage: string = error.message;
            let alert = this.alertCtrl.create({
              message: errorMessage,
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        });
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange: true
      });
      // console.time('loading started');
      this.loading.present();

      /*console.log('personalInfo '); console.log(this.personalInfo);
      console.log('bankInfo '); console.log(this.bankInfo);
      console.log('carInfo '); console.log(this.carInfo);
      console.log('insuranceInfo '); console.log(this.insuranceInfo);
      console.log('legalInfo '); console.log(this.legalInfo);*/
    }
  }

}
