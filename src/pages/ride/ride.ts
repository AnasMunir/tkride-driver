import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { Geolocation, Geoposition, NativeStorage } from 'ionic-native';

import { HomePage } from '../home/home';
import { ExtraCostPage } from '../extra-cost/extra-cost';
import { EndRideModalPage } from '../end-ride-modal/end-ride-modal';

import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

// importing providers
import { FcmNotification } from '../../providers/fcm-notification';
import { Payment } from '../../providers/payment';

import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

declare var GeoFire: any;
declare var google;

@Component({
  selector: 'page-ride',
  templateUrl: 'ride.html'
})
export class RidePage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  driverMarker: any;
  public disabled: boolean = true;
  public disabledStart: boolean = true; public disabledEnd: boolean = true;
  pickupLat: number;
  pickupLng: number;
  clientToken: any; uid: any; userUid: any;
  startTime: any; endTime: any;
  extraCost: boolean = false;

  paymentData = {
    cc: '',
    cvv: '',
    month: '',
    year: '',
    amount: ''
  }

  drivers$: FirebaseObjectObservable<any>;
  users$: FirebaseObjectObservable<any>;

  constructor(public navCtrl: NavController,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public af: AngularFire,
    public fcmNotification: FcmNotification,
    public payment: Payment) {


    this.pickupLat = this.navParams.get('pickupLat');
    this.pickupLng = this.navParams.get('pickupLng');
    this.clientToken = this.navParams.get('clientToken');
    this.uid = this.navParams.get('uid');
    this.userUid = this.navParams.get('userUid');
    console.log("pickUp lat above: " + this.pickupLat);

    let driver = firebase.auth().currentUser;
    let ref = firebase.database().ref('drivers/locations/' + driver.uid);
    // menu.enable(true);
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });

    Geolocation.getCurrentPosition().then((pos) => {
      console.log('pos :' + pos);
      let driverLatLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      let pickupLatLng = new google.maps.LatLng(this.pickupLat, this.pickupLng);

      let mapOptions = {
        center: driverLatLng,
        scroll: true,
        rotate: true,
        zoomControl: false,
        disableDefaultUI: true,
        mapTypeControl: false,
        zoom: 15,
        tilt: 30,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let driverImage = {
        url: 'assets/img/car.png',
        size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(50, 50)
      };
      let personImage = {
        url: 'assets/img/person.png',
        size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(50, 50)
      };
      this.driverMarker = new google.maps.Marker({
        map: this.map,
        animation: null,
        position: driverLatLng,
        icon: driverImage
      });
      let personMarker = new google.maps.Marker({
        map: this.map,
        animation: null,
        position: pickupLatLng,
        icon: personImage
      });

      directionsDisplay.setMap(this.map);

      directionsService.route({
        origin: driverLatLng,
        destination: pickupLatLng,
        travelMode: 'DRIVING'
      }, function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          console.log('Directions request failed due to ' + status);
        }
      });
    });

    ref.on("value", (snap) => {
      let val = snap.val();
      // this.driverLat = val.l[0];
      // this.driverLng = val.l[1];
      let markerPos = new google.maps.LatLng(val.l[0], val.l[1]);
      this.driverMarker.setPosition(markerPos);

    });

    let driverRef = firebase.database().ref('drivers/locations');
    // driverRef.on("value", (snap) => {
    //   console.log(snap.val());
    // });

    this.drivers$ = this.af.database.object('drivers/');

    let geoFire = new GeoFire(driverRef);


    let geoQuery = geoFire.query({
      center: [Number(this.pickupLat), Number(this.pickupLng)],
      radius: 0.5
    })


    geoQuery.on("key_entered", key => {
      this.drivers$.first().subscribe(snapshot => {
        console.log("Driver has arrived to the pickup location");
        console.log(snapshot[key]);
        // this.disabled = false;
        this.disabledEnd = false; this.disabledStart = false;
        console.log('yo, driver has arrived');
      });
    });

  }

  ionViewDidLoad() {
    console.log('Hello RidePage Page');
    // let driverRef = firebase.database().ref('drivers/locations');
    // let geoFire = new GeoFire(driverRef);
    // let geoQuery = geoFire.query({
    //   center: [this.pickupLat, this.pickupLng],
    //   radius: 0.5
    // });
  }

  startRide() {
    // this.fcmNotification.startRideNotification(this.clientToken, this.uid, true);
    this.startTime = new Date();
    let startTime = new Date();
    // let time = startTime.getHours() + ':' + startTime.getMinutes(); + ':' + startTime.getSeconds();
    let startRide = this.alertCtrl.create({
      title: 'Start ride',
      // message: '$' + (tempFair),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.disabledStart = false;
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.disabledStart = true;
            this.fcmNotification.startRideNotification(this.clientToken, this.uid, true);
          }
        }
      ]
    });
    startRide.present();
  }

  endRide() {
    let confirm = this.alertCtrl.create({
      title: 'End ride?',
      // message: '$' + (tempFair),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // this.disabledStart = false;
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            console.log('OK clicked');
            this.disabledEnd = true;
            this.fcmNotification.endRideNotificaiton(this.clientToken, this.uid, false);
            this.endTime = new Date();
            let timeLapsed = ((this.endTime.getHours() - this.startTime.getHours()) * 3600) + ((this.endTime.getMinutes() - this.startTime.getMinutes()) * 60) + (this.endTime.getSeconds() - this.startTime.getSeconds());

            console.log('userUid: ' + this.userUid);
            let userRef = firebase.database().ref('users/' + this.userUid);

            userRef.on("value", (snap) => {

              this.paymentData.cc = snap.val().cc;
              this.paymentData.cvv = snap.val().cvv;
              this.paymentData.month = snap.val().month;
              this.paymentData.year = snap.val().year;

            });
            let self = this;

            Geolocation.getCurrentPosition().then((pos) => {
              console.log('pos :' + pos);
              let currentPosition = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
              let pickupLatLng = new google.maps.LatLng(this.pickupLat, this.pickupLng);
              let service = new google.maps.DistanceMatrixService;

              service.getDistanceMatrix({
                origins: [pickupLatLng],
                destinations: [currentPosition],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.IMPERIAL,
                avoidHighways: false,
                avoidTolls: false
              }, function (response, status) {
                if (status !== 'OK') {
                  alert('Error was: ' + status);
                } else {
                  console.log(response);

                  let distance = response.rows[0].elements[0].distance.value;
                  let duration = response.rows[0].elements[0].duration.value;

                  let rideDistance = response.rows[0].elements[0].distance.text;
                  let rideDuration = response.rows[0].elements[0].duration.text;

                  // self.fcmNotification.endRideNotificaiton(this.clientToken, this.uid, rideDistance, false);

                  console.log('distance: ' + distance);
                  console.log('duration: ' + duration);
                  console.log('time lapsed: ' + timeLapsed);

                  let totalFair = 12 + (0.00170878 * distance) + (0.0083333 * timeLapsed);
                  //totalFair = totalFair;
                  let totalFairRounded = Math.round(totalFair);
                  let tempFair = totalFairRounded;
                  let totalFairRoundedCents = totalFairRounded * 100;
                  // this.totalFairRounded =
                  // self.fcmNotification.endRideNotificaiton(this.clientToken, pickupLatLng, currentPosition, rideDistance, timeLapsed, totalFairRounded, this.uid, false);
                  console.log('total fair: ' + totalFair);

                  let endRide = self.modalCtrl.create(EndRideModalPage,
                    {
                      distance: rideDistance,
                      duration: self.endTime.getMinutes() /*timeLapsed*/,
                      totalFair: totalFairRounded
                    });
                  endRide.present();


                  endRide.onDidDismiss(data => {
                    if (data === true) {
                      this.extraCost = true;
                      self.saveRideForUser(pickupLatLng, currentPosition, rideDistance, timeLapsed, totalFairRounded);
                      self.saveRide(pickupLatLng, currentPosition, rideDistance, timeLapsed, totalFairRounded); // saving ride in server &
                      self.paymentData.amount = totalFairRounded.toString();

                      let resMaan = self.payment.sendPaymentToMaan(self.paymentData);
                      resMaan.subscribe(res => {
                        console.log('result from maanServer');
                        console.log(res);
                      });
                    }
                    else {
                      this.extraCost = false;
                      self.saveRideForUser(pickupLatLng, currentPosition, rideDistance, timeLapsed, totalFairRounded);
                      self.saveRide(pickupLatLng, currentPosition, rideDistance, timeLapsed, totalFairRounded); // saving ride in server &
                      self.paymentData.amount = totalFairRounded.toString();

                      let resMaan = self.payment.sendPaymentToMaan(self.paymentData);
                      resMaan.subscribe(res => {
                        console.log('result from maanServer');
                        console.log(res);
                      });
                    }
                    self.navCtrl.setRoot(HomePage);
                  })

                  let confirm = self.alertCtrl.create({
                    title: 'Ride ended',
                    subTitle: 'Distance: ' + rideDistance + ' Duration: ' + timeLapsed,
                    message: ' Total fair: $' + (tempFair),
                    buttons: [
                      {
                        text: 'OK',
                        handler: () => {
                          console.log('OK clicked');
                          self.navCtrl.setRoot(HomePage);
                        }
                      }]
                  });
                  // confirm.present();

                  // return dataForUser;
                }
              });
            }).catch(err => console.log);
          }
        }]
    });
    confirm.present();
  }

  saveRide(pickupLocation, dropoffLocation, distance, duration, totalFair) {

    let driver = firebase.auth().currentUser;
    let ref = firebase.database().ref('drivers/' + driver.uid);
    this.drivers$ = this.af.database.object('drivers/' + driver.uid, { preserveSnapshot: true });
    let latLng = new google.maps.LatLng(pickupLocation);
    // console.log('in latlng: '); console.log(latLng);

    // const promise = this.af.database.object('drivers/'+driver.uid, { preserveSnapshot: true });

    this.drivers$.first().subscribe(snap => {
      let numberOfRides = snap.val().numberOfRides;
      console.log('number of rides'); console.log(numberOfRides);
      if (numberOfRides) {
        let totalRides = parseInt(numberOfRides) + 1;

        ref.update({
          numberOfRides: totalRides
        }).then(() => {
          ref.child('rideHistory').child(totalRides.toString()).update({
            // dropoffLocation: dropoffLocation,
            distance: distance,
            duration: duration,
            totalFair: totalFair,
            extraCost: this.extraCost,
            // points: totalFair,
            date: new Date()
          }).then(() => {
            this.geocode(pickupLocation)
              .then(pickup => {
                console.log('pickup from promise ');
                console.log(pickup);
                // return pickup;
                ref.child('rideHistory').child(totalRides.toString()).update({
                  pickupLocation: pickup,
                }).then(() => {
                  this.geocode(dropoffLocation)
                    .then(dropoff => {
                      console.log('dropoff from promise ');
                      console.log(dropoff);
                      ref.child('rideHistory').child(totalRides.toString()).update({
                        dropffLocation: dropoff,
                      }).catch(err => console.log);
                    }).catch(err => console.log);
                }).catch(err => console.log);
              }).catch(err => console.log);
          }).catch(err => console.log);

        });

      } else {
        ref.update({
          numberOfRides: 1
        }).then(() => {
          ref.child('rideHistory').child('1').update({
            // dropoffLocation: dropoffLocation,
            distance: distance,
            duration: duration,
            totalFair: totalFair,
            extraCost: this.extraCost,
            // points: totalFair,
            date: new Date()
          }).then(() => {
            this.geocode(pickupLocation)
              .then(pickup => {
                console.log('pickup from promise ');
                console.log(pickup);
                // return pickup;
                ref.child('rideHistory').child('1').update({
                  pickupLocation: pickup,
                }).then(() => {
                  this.geocode(dropoffLocation)
                    .then(dropoff => {
                      console.log('dropoff from promise ');
                      console.log(dropoff);
                      ref.child('rideHistory').child('1').update({
                        dropffLocation: dropoff,
                      }).catch(err => console.log);
                    }).catch(err => console.log);
                }).catch(err => console.log);
              }).catch(err => console.log);
          }).then(() => {
            // self.saveRideForUser(pickupLocation, dropoffLocation, distance, duration, totalFair);
          }).catch(err => console.log);

        });
      }
    });
  }

  saveRideForUser(pickupLocation, dropoffLocation, distance, duration, totalFair) {

    // let userRef = firebase.database().ref('users/' + this.userUid);

    this.users$ = this.af.database.object('users/' + this.userUid.toString(), { preserveSnapshot: true });
    let driver = firebase.auth().currentUser;
    let ref = firebase.database().ref('users/' + this.userUid.toString());
    // this.drivers$ = this.af.database.object('drivers/' + driver.uid, { preserveSnapshot: true });
    // let latLng = new google.maps.LatLng(pickupLocation);
    // console.log('in latlng: '); console.log(latLng);

    // const promise = this.af.database.object('drivers/'+driver.uid, { preserveSnapshot: true });

    this.users$.first().subscribe(snap => {
      let numberOfRides = snap.val().numberOfRides;
      console.log('number of rides in user'); console.log(numberOfRides);
      if (numberOfRides) {
        let totalRides = parseInt(numberOfRides) + 1;

        ref.update({
          numberOfRides: totalRides
        }).then(() => {
          ref.child('rideHistory').child(totalRides.toString()).update({
            // dropoffLocation: dropoffLocation,
            distance: distance,
            duration: duration,
            totalFair: totalFair,
            extraCost: this.extraCost,
            points: totalFair + ' points',
            date: new Date()
          }).then(() => {
            this.geocode(pickupLocation)
              .then(pickup => {
                console.log('pickup from promise ');
                console.log(pickup);
                // return pickup;
                ref.child('rideHistory').child(totalRides.toString()).update({
                  pickupLocation: pickup,
                }).then(() => {
                  this.geocode(dropoffLocation)
                    .then(dropoff => {
                      console.log('dropoff from promise ');
                      console.log(dropoff);
                      ref.child('rideHistory').child(totalRides.toString()).update({
                        dropffLocation: dropoff,
                      }).catch(err => console.log);
                    }).catch(err => console.log);
                }).catch(err => console.log);
              }).catch(err => console.log);
          }).catch(err => console.log);

        });

      } else {
        ref.update({
          numberOfRides: 1
        }).then(() => {
          ref.child('rideHistory').child('1').update({
            // dropoffLocation: dropoffLocation,
            distance: distance,
            duration: duration,
            totalFair: totalFair,
            extraCost: this.extraCost,
            points: totalFair + ' points',
            date: new Date()
          }).then(() => {
            this.geocode(pickupLocation)
              .then(pickup => {
                console.log('pickup from promise ');
                console.log(pickup);
                // return pickup;
                ref.child('rideHistory').child('1').update({
                  pickupLocation: pickup,
                }).then(() => {
                  this.geocode(dropoffLocation)
                    .then(dropoff => {
                      console.log('dropoff from promise ');
                      console.log(dropoff);
                      ref.child('rideHistory').child('1').update({
                        dropffLocation: dropoff,
                      }).catch(err => console.log);
                    }).catch(err => console.log);
                }).catch(err => console.log);
              }).catch(err => console.log);
          }).then(() => {
            // self.saveRideForUser(pickupLocation, dropoffLocation, distance, duration, totalFair);
          }).catch(err => console.log);

        });
      }
    });
  }
  geocode(latLng): Promise<any> {
    return new Promise(resolve => {
      let geocoder = new google.maps.Geocoder;
      // let latLng = new google.maps.LatLng(latlng);
      console.log(latLng);
      let self = this;
      geocoder.geocode({ 'location': latLng }, function (results, status) {
        if (status === 'OK') {
          // resolve(true);
          let location = results[0]['address_components'][0].long_name + ' ' + results[1]['address_components'][0].long_name + ' ' + results[0]['address_components'][1].long_name;
          console.log('pickup from function ');
          console.log(location);
          resolve(location);
          return location;
        } else {
          console.log('Geocoder failed due to: ' + status);
          resolve(false);
        }
      });
    })
  }

  addCost() {
    let modal = this.modalCtrl.create(ExtraCostPage);
    modal.present();

    modal.onDidDismiss(data => {
      if (data) {
        this.extraCost = true;
      }
      else {
        this.extraCost = false;
      }
    })
  }

}
