import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AngularFire, /*FirebaseListObservable,*/ FirebaseObjectObservable} from 'angularfire2';

declare var google;

@Component({
  selector: 'page-ride-history',
  templateUrl: 'ride-history.html'
})
export class RideHistoryPage {

  // items: FirebaseListObservable<any>;
  objects: FirebaseObjectObservable<any>;
  rides: any
  pickup: any;
  data: boolean = false;

  constructor(public navCtrl: NavController, public af: AngularFire) {

    let driver = firebase.auth().currentUser;
    let geocoder = new google.maps.Geocoder;
    /*this.items = af.database.list('drivers/' + driver.uid, { preserveSnapshot: true });
    this.items.subscribe(snap => {
      console.log('form lists');
      console.log(snap);
    })*/
    this.objects = af.database.object('drivers/' + driver.uid, { preserveSnapshot: true });
    this.objects.first().subscribe(snap => {
      this.data = true;
      console.log('from objects');
      console.log(snap.val().rideHistory);
      this.rides = snap.val().rideHistory;
      // console.log(JSON.parse(this.rides.pickuplocation));
      // this.pickup = this.geocode(this.rides.pickupLocation);
      // console.log(this.pickup);
    })
  }

  ionViewDidLoad() {
    console.log('Hello RideHistoryPage Page');
  }

}
