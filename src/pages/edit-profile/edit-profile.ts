import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {
  submitAttempt: boolean = false;
  profileForm: any;
  fullname: any; email: any; city: any; state: any; zipcode: any; phoneNumber: any;
  ssn: any; bankName: any; accountHolderName: any; abaNumber: any; accountNumber: any;
  drivingLicense: any; expirationDate: any; licensePlateNumber: any;
  carMake: any; carBrand: any; carColor: any; carYear: any;
  insuranceName: any; policyNumber: any; tcpNumber: any;
  driveForUber: any; driveForLyft: any; driveForLimo: any; ownLimo: any;
  profilePhoto: any; profilePhotoRef: any; profilePhotoCheck: any;
  carPicture: any; insuranceCardPhoto: any; dmvPicture: any; carRegistrationPicture: any;
  photoRef: any; carRegPhotoRef: any; dmvPhotoRef: any; insuranceCardPhotoRef: any;
  photoRefCheck: any; carRegPhotoRefCheck: any; dmvPhotoRefCheck: any; insuranceCardPhotoRefCheck: any;
  loading: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController) {

    this.fullname = this.navParams.get('fullName');
    this.city = this.navParams.get('city');
    this.state = this.navParams.get('state');
    this.zipcode = this.navParams.get('zipcode');
    this.phoneNumber = this.navParams.get('phoneNumber');
    this.ssn = this.navParams.get('ssn');
    this.bankName = this.navParams.get('bankName');
    this.accountHolderName = this.navParams.get('accountHolderName');
    this.abaNumber = this.navParams.get('abaNumber');
    this.drivingLicense = this.navParams.get('drivingLicense');
    this.expirationDate = this.navParams.get('expirationDate');
    this.licensePlateNumber = this.navParams.get('licensePlateNumber');
    this.carMake = this.navParams.get('carMake');
    this.carBrand = this.navParams.get('carBrand');
    this.carColor = this.navParams.get('carColor');
    this.carYear = this.navParams.get('carYear');
    this.insuranceName = this.navParams.get('insuranceName');
    this.policyNumber = this.navParams.get('policyNumber');
    this.driveForUber = this.navParams.get('driveForUber');
    this.driveForLyft = this.navParams.get('driveForLyft');
    this.driveForLimo = this.navParams.get('driveForLimo');
    this.ownLimo = this.navParams.get('ownLimo');
    this.carPicture = this.navParams.get('carPicture');
    this.dmvPicture = this.navParams.get('dmvPicture');
    this.carRegistrationPicture = this.navParams.get('carRegistrationPicture');
    this.insuranceCardPhoto = this.navParams.get('insuranceCardPhoto');
    this.profilePhoto = navParams.get('profilePhoto');

    let ssnPatern = "d{3}-?\d{2}-?\d{4}";
    this.profileForm = formBuilder.group({
      // firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      // lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      fullName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*')])],
      city: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*')])],
      state: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*')])],
      // billingAddress: [''],
      zipcode: ['', Validators.compose([Validators.maxLength(5), Validators.pattern('[0-9]*')])],
      phoneNumber: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]*')])],
      ssn: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(9), Validators.pattern('[0-9]*')])],

      bankName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*')])],
      accountHolderName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*')])],
      accountNumber: ['', Validators.compose([Validators.minLength(16), Validators.maxLength(16), Validators.pattern('[0-9]*')])],
      abaNumber: ['', Validators.compose([Validators.pattern('[0-9]*')])],

      drivingLicense: [''],
      expirationDate: [''],
      licensePlateNumber: ['', Validators.compose([Validators.maxLength(10)])],
      carMake: [''],
      carBrand: [''],
      carYear: [''],
      carColor: [''],

      insuranceName: [''],
      policyNumber: [''],

      tcpNumber: [''],
      driveForUber: [''],
      driveForLyft: [''],
      driveForLimo: [''],
      ownLimo: ['']
    });

  }

  ionViewDidLoad() {
    console.log('Hello EditProfilePage Page');

  }
  updateProfile() {
    this.submitAttempt = true;
    if (!this.profileForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.profileForm.value);
      let driver = firebase.auth().currentUser;
      let ref = firebase.database().ref('drivers/' + driver.uid);
      let storageRef = firebase.storage().ref('/drivers/');

      if (this.profileForm.value.fullName) {
        ref.update({ fullname: this.profileForm.value.fullName })
      }
      if (this.profileForm.value.carMake) {
        ref.update({ car_make: this.profileForm.value.carMake })
      }
      if (this.profileForm.value.carBrand) {
        ref.update({ car_modal: this.profileForm.value.carBrand })
      }
      if (this.profileForm.value.carColor) {
        ref.update({ car_color: this.profileForm.value.carColor })
      }
      if (this.profileForm.value.carYear) {
        ref.update({ car_year: this.profileForm.value.carYear })
      }
      if (this.profileForm.value.drivingLicense) {
        ref.update({ drivingLicense: this.profileForm.value.drivingLicense })
      }
      if (this.profileForm.value.phoneNumber) {
        ref.update({ phonenumber: this.profileForm.value.phoneNumber })
      }
      if (this.profilePhotoCheck === true) {
        storageRef.child(driver.uid).child('driver.png')
          .putString(this.profilePhotoRef, 'base64', { contentType: 'image/png' })
          .then((savedPicture) => {
            console.log('photo RESPONSES')
            console.log(savedPicture)
            ref.update({ carPicture: savedPicture.downloadURL })
          }).catch(err => { console.log(err) });
      }
      if (this.photoRefCheck === true) {
        storageRef.child(driver.uid).child('carProfile.png')
          .putString(this.photoRef, 'base64', { contentType: 'image/png' })
          .then((savedPicture) => {
            console.log('photo RESPONSES')
            console.log(savedPicture)
            ref.update({ carPicture: savedPicture.downloadURL })
          }).catch(err => { console.log(err) });
      }
      if (this.carRegPhotoRefCheck === true) {
        storageRef.child(driver.uid).child('carRegistration.png')
          .putString(this.carRegPhotoRef, 'base64', { contentType: 'image/png' })
          .then((savedPicture) => {
            console.log('photo RESPONSES')
            console.log(savedPicture)
            ref.update({ carRegistrationPicture: savedPicture.downloadURL })
          }).catch(err => { console.log(err) });
      }
      if (this.dmvPhotoRefCheck === true) {
        storageRef.child(driver.uid).child('dmv.png')
          .putString(this.dmvPhotoRef, 'base64', { contentType: 'image/png' })
          .then((savedPicture) => {
            console.log('photo RESPONSES')
            console.log(savedPicture)
            ref.update({ dmvPicture: savedPicture.downloadURL })
          }).catch(err => { console.log(err) });
      }
      if (this.insuranceCardPhotoRefCheck === true) {
        storageRef.child(driver.uid).child('insuranceCard.png')
          .putString(this.insuranceCardPhotoRef, 'base64', { contentType: 'image/png' })
          .then((savedPicture) => {
            console.log('photo RESPONSES')
            console.log(savedPicture)
            ref.update({ insuranceCardPhoto: savedPicture.downloadURL })
          }).catch(err => { console.log(err) });
      }

      this.navCtrl.pop();
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }
  takeProfilePicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.profilePhotoRef = imageData;
      this.profilePhotoCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }
  takeProfilePictureFromPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.profilePhotoRef = imageData;
      this.profilePhotoCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takePicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.photoRef = imageData;
      this.profilePhotoCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takePictureFromPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.photoRef = imageData;
      this.photoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takeCarRegistrationPicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.carRegPhotoRef = imageData;
      this.carRegPhotoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takeCarRegistrationPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.carRegPhotoRef = imageData;
      this.carRegPhotoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takeDmvPicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.dmvPhotoRef = imageData;
      this.dmvPhotoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takeDmvPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.dmvPhotoRef = imageData;
      this.dmvPhotoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takeInsurancePicture() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.insuranceCardPhotoRef = imageData;
      this.insuranceCardPhotoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  takeInsurancePictureFromPhotos() {
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 300,
      targetHeight: 300
      // saveToPhotoAlbum: true
    }).then(imageData => {
      console.log('photo uploaded');
      console.log(imageData);
      this.insuranceCardPhotoRef = imageData;
      this.insuranceCardPhotoRefCheck = true;
      let loading = this.loadingCtrl.create({
        content: "Uploading photo",
        duration: 1000
      });
      loading.present();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

}
