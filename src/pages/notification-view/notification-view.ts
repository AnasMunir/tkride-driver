import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';

/*
  Generated class for the NotificationView page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-notification-view',
  templateUrl: 'notification-view.html'
})
export class NotificationViewPage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParam: NavParams) {
    let parameter1 = navParam.get('param1');
    console.log(parameter1);
    let confirm = this.alertCtrl.create({
      title: parameter1,
      message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    // confirm.present();
  }

  ionViewDidLoad() {
    console.log('Hello NotificationViewPage Page');
  }

}
