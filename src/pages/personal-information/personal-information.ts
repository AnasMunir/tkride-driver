import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators, FormControl, FormControlName } from '@angular/forms';
import { NativeStorage } from 'ionic-native';

import { BankInformaionPage } from '../bank-informaion/bank-informaion';

@Component({
  selector: 'page-personal-information',
  templateUrl: 'personal-information.html'
})
export class PersonalInformationPage {

  public signupPersonalForm: any;
  email: any; password: any;
  submitAttempt: boolean = false;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public navParams: NavParams) {

    /*this.email = this.navParams.get('email');
    this.password = this.navParams.get('password');*/

    let ssnPatern = "d{3}-?\d{2}-?\d{4}";
    this.signupPersonalForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      city: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      state: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      billingAddress: ['', Validators.required],
      zipcode: ['', Validators.compose([Validators.maxLength(5), Validators.pattern('[0-9]*'), Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]*'), Validators.required])],

      ssn: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(9),Validators.pattern('[0-9]*'), Validators.required])],
    })

  }

  ionViewDidLoad() {
    console.log('Hello PersonalInformationPage Page');
    NativeStorage.getItem('user').then(data => {
      console.log(data);
    }).catch(err => {console.log(err)})
  }

  continue() {
    this.submitAttempt = true;
    if (!this.signupPersonalForm.valid) {
      console.log('something went wrong')
    } else {
      NativeStorage.setItem('personalInfo',
      {
        firstName: this.signupPersonalForm.value.firstName,
        lastName: this.signupPersonalForm.value.lastName,
        city: this.signupPersonalForm.value.city,
        state: this.signupPersonalForm.value.state,
        zipcode: this.signupPersonalForm.value.zipcode,
        phoneNumber: this.signupPersonalForm.value.phoneNumber,
        ssn: this.signupPersonalForm.value.ssn
      })
      .catch((err) => { console.log(err) });

      this.navCtrl.push(BankInformaionPage);
    }
  }

}
