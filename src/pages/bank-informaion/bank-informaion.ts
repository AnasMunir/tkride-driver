import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators, FormControl, FormControlName } from '@angular/forms';
import { NativeStorage } from 'ionic-native';

import { CarInformationPage } from '../car-information/car-information';

@Component({
  selector: 'page-bank-informaion',
  templateUrl: 'bank-informaion.html'
})
export class BankInformaionPage {

  public signupBankForm: any;
  submitAttempt: boolean = false;
  email: any; password: any; firstName: any; lastName: any; city: any;
  state: any; zipcode: any; phoneNumber: any; ssn: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder) {

    this.signupBankForm = formBuilder.group({
      bankName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      accountHolderName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      accountNumber: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(30), Validators.pattern('[0-9]*'), Validators.required])],
      abaNumber: ['', Validators.compose([Validators.pattern('[0-9]*'), Validators.required])],

    })
  }

  ionViewDidLoad() {
    console.log('Hello BankInformaionPage Page');
    NativeStorage.getItem('personalInfo').then(data => {
      console.log(data);
    }).catch(err => {console.log(err)});
    NativeStorage.getItem('user').then(data => {
      console.log(data);
    }).catch(err => {console.log(err)});
  }

  continue() {
    this.submitAttempt = true;
    if (!this.signupBankForm.valid) {
      console.log('something went wrong')
    } else {

      NativeStorage.setItem('bankInfo', {
        bankName: this.signupBankForm.value.bankName,
        accountHolderName: this.signupBankForm.value.accountHolderName,
        accountNumber: this.signupBankForm.value.accountNumber,
        abaNumber: this.signupBankForm.value.abaNumber
      }).catch(err => {console.log(err)});

      this.navCtrl.push(CarInformationPage);
    }
  }

}
