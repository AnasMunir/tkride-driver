import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, LoadingController } from 'ionic-angular';
import { StatusBar, Splashscreen, Keyboard, BackgroundGeolocation, NativeStorage } from 'ionic-native';

// importing pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { AboutPage } from '../pages/about/about';
import { RideHistoryPage } from '../pages/ride-history/ride-history';
import { SupportPage } from '../pages/support/support';
// import { CarInformationPage } from '../pages/car-information/car-information';
// import { LegalInformationPage } from '../pages/legal-information/legal-information';
// import { CarInformationPage } from '../pages/car-information/car-information';
import { InsuranceInformationPage } from '../pages/insurance-information/insurance-information';
// importing provider
import { AuthData } from '../providers/auth-data';

import { AngularFire } from 'angularfire2';

declare var cordova;
@Component({
  templateUrl: 'app.html'

})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  public pages: Array<{ title: string, component: any }>;

  constructor(platform: Platform,
    public af: AngularFire,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public auth: AuthData) {

    Keyboard.hideKeyboardAccessoryBar(false);

    this.pages = [
      { title: 'My Location', component: HomePage },
      { title: 'About', component: AboutPage },
      { title: 'Profile', component: ProfilePage },
      { title: 'Ride history', component: RideHistoryPage },
      { title: 'Support', component: SupportPage }
    ];

    platform.ready().then(() => {
      let env = this;
      /*NativeStorage.getItem('user')
        .then(data => {
          // user is previously logged and we have his data
          // we will let him access the app
          console.log(data);
          env.nav.setRoot(HomePage);
        },error => {
          //we don't have the user data so we will ask him to log in
          env.nav.setRoot(LoginPage);
          Splashscreen.hide();
        }).catch(err => { console.log(err) });*/

      af.auth.subscribe(user => {
        if (user) {
          this.rootPage = HomePage;
          af.auth.unsubscribe();
        } else {
          this.rootPage = LoginPage;
          af.auth.unsubscribe();
        }
      });
      Splashscreen.hide();
      StatusBar.styleDefault();

    });
  }
  openPage(page) {

    this.menuCtrl.close();
    this.nav.setRoot(page.component);
  }
  logout() {
    this.menuCtrl.close();
    this.nav.setRoot(LoginPage);
    this.af.auth.logout();
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
    });
    loading.present();
  }
}
