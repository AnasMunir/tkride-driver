import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// importing pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ModalPage } from '../pages/modal-page/modal-page';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { NotificationViewPage } from '../pages/notification-view/notification-view';
import { RequestModalPage } from '../pages/request-modal/request-modal';
import { TermsConditionsPage } from '../pages/terms-conditions/terms-conditions';
import { ProfilePage } from '../pages/profile/profile';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { RidePage } from '../pages/ride/ride';
import { AboutPage } from '../pages/about/about';
import { PersonalInformationPage } from '../pages/personal-information/personal-information';
import { BankInformaionPage } from '../pages/bank-informaion/bank-informaion';
import { CarInformationPage } from '../pages/car-information/car-information';
import { LegalInformationPage } from '../pages/legal-information/legal-information';
import { InsuranceInformationPage } from '../pages/insurance-information/insurance-information';
import { RideHistoryPage } from '../pages/ride-history/ride-history';
import { SupportPage } from '../pages/support/support';
import { WelcomePage } from '../pages/welcome/welcome';
import { ExtraCostPage } from '../pages/extra-cost/extra-cost'
import { EndRideModalPage } from '../pages/end-ride-modal/end-ride-modal';
// importing providers
import { AuthData } from '../providers/auth-data';
import { FcmNotification } from '../providers/fcm-notification';
import { Connectivity } from '../providers/connectivity';
import { GoogleMaps } from '../providers/google-maps';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { Payment } from '../providers/payment';
// Import the AF2 Module
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { IonicImageLoader } from 'ionic-image-loader';

export const firebaseConfig = {
  apiKey: "AIzaSyAPl8CEm1VBFfaAZ-B0fY_CEyR3PUXmm-c",
  authDomain: "user-tracking-af847.firebaseapp.com",
  databaseURL: "https://user-tracking-af847.firebaseio.com",
  storageBucket: "user-tracking-af847.appspot.com",
  messagingSenderId: "840819858389"
};

// Configuring Firebase auth
const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ModalPage,
    ResetPasswordPage,
    SignUpPage,
    NotificationViewPage,
    RequestModalPage,
    TermsConditionsPage,
    ProfilePage,
    EditProfilePage,
    RidePage,
    AboutPage,
    PersonalInformationPage,
    BankInformaionPage,
    CarInformationPage,
    LegalInformationPage,
    InsuranceInformationPage,
    RideHistoryPage,
    SupportPage,
    WelcomePage,
    ExtraCostPage,
    EndRideModalPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      platforms: {
        ios: {
          // These options are available in ionic-angular@2.0.0-beta.2 and up.
          scrollAssist: false,    // Valid options appear to be [true, false]
          autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
        },
        android: {
          // These options are available in ionic-angular@2.0.0-beta.2 and up.
          scrollAssist: false,    // Valid options appear to be [true, false]
          autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
        }
      }
    }),
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyCFAL58sFPY1c99GA-AuEhjDxIMDrKT_kY' }),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig),
    IonicImageLoader
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ModalPage,
    ResetPasswordPage,
    SignUpPage,
    NotificationViewPage,
    RequestModalPage,
    TermsConditionsPage,
    ProfilePage,
    EditProfilePage,
    RidePage,
    AboutPage,
    PersonalInformationPage,
    BankInformaionPage,
    CarInformationPage,
    LegalInformationPage,
    InsuranceInformationPage,
    RideHistoryPage,
    SupportPage,
    WelcomePage,
    ExtraCostPage,
    EndRideModalPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }, AuthData, FcmNotification, Connectivity, GoogleMaps, Payment]
})
export class AppModule { }
