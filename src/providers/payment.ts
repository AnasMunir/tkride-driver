import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';


@Injectable()
export class Payment {

  constructor(public http: Http) {
    console.log('Hello Payment Provider');
  }

  sendPaymentToMaan(paymentData: any) {
    let maanURL = 'https://butler247.com/tkride_payment/checkout';

    console.log('paymentData');
    console.log(paymentData);
    let body = new URLSearchParams(paymentData);
    body.set('number', paymentData.cc);
    body.set('exp_month', paymentData.month);
    body.set('exp_year', paymentData.year);
    body.set('amount', paymentData.amount);
    body.set('app_key', 'from_tk');

    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers});

    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
    // .map((res:any) => res.json())
    //...errors if any
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
