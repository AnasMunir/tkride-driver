import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { AlertController } from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
import 'rxjs/add/operator/map';


@Injectable()
export class AuthData {
  fireAuth: any;
  drivers$: FirebaseObjectObservable<any>;
  user: any; email: any; password: any;
  personalInfo: any; firstName: any; lastName: any; city: any; state: any; zipcode: any; phoneNumber: any; ssn: any;
  bankInfo: any; bankName: any; accountHolderName: any; accountNumber: any; abaNumber: any;
  carInfo: any; drivingLicense: any; expirationDate: any; licensePlateNumber: any; carMake: any; carBrand: any; carYear: any; carColor: any; photoRef: any; carRegPhotoRef: any; dmvPhotoRef: any;
  insuranceInfo: any; insuranceName: any; policyNumber: any; insuranceCardPhoto: any;
  legalInfo: any; tcpNumber: any; driveForUber: any; driveForLyft: any; driveForLimo: any; ownLimo: any;
  regUser: any;

  constructor(public af: AngularFire, public alertCtrl: AlertController) { }
  loginUser(newEmail: string, newPassword: string): any {
    return this.af.auth.login({ email: newEmail, password: newPassword })
  }
  resetPassword(email: string): any {
    return firebase.auth().sendPasswordResetEmail(email);
  }
  logoutUser(): any {
    return firebase.auth().signOut();
  }
  createUser(email: string, password: string, driverPhoto: any): firebase.Promise<any> {
    // return firebase.auth().getRedirectResult();
    return this.af.auth.createUser({ email: email, password: password })
      .then(regUser => {
        this.regUser = regUser;
        let ref = firebase.database().ref('/');
        let storageRef = firebase.storage().ref('/drivers/');
        storageRef.child(regUser.uid).child('driver.png')
          .putString(driverPhoto, 'base64', { contentType: 'image/png' })
          .then(savedPicture => {
            ref.child('drivers').child(regUser.uid).update({
              email: email,
              driverPicture: savedPicture.downloadURL
            })
          });
      })
  }

  signUpUser(
    // email: string,
    password: string,
    firstName: string,
    lastName: string,
    // newStreetAddress: string,
    city: string,
    state: string,
    zipcode: string,
    phoneNumber: string,
    ssn: string,
    bankName: string,
    accountNumber: string,
    abaNumber: string,
    accountHolderName: string,
    drivingLicense: string,
    expirationDate: string,
    licensePlateNumber: string,
    carMake: string,
    carBrand: string,
    carYear: string,
    carColor: string,
    photoRef: any,
    carRegPhotoRef: any,
    dmvPhotoRef: any,
    insuranceInfo: string,
    insuranceName: string,
    policyNumber: string,
    insuranceCardPhoto: any,
    tcpNumber: string,
    driveForUber: string,
    driveForLyft: string,
    driveForLimo: string,
    ownLimo: string): Promise<any> {
    // return firebase.auth().createUserWithEmailAndPassword(newEmail, newPassword);
    return new Promise(resolve => {

      /*this.af.auth.createUser(
        {
          email: this.email,
          password: this.password
        })*/
      let regUser = this.regUser
      console.log('regUser '); console.log(regUser);
      let bharwaaa = firebase.auth().currentUser;
      console.log('bharwaa '); console.log(bharwaaa);
      var ref = firebase.database().ref('drivers/' + regUser.uid);
      let storageRef = firebase.storage().ref('/drivers/');
      // let storageRef = storage.ref();
      console.log('the photo reference: ');
      // console.log(photoRef);
      storageRef.child(regUser.uid).child('carProfile.png')
        .putString(photoRef, 'base64', { contentType: 'image/png' })
        .then((savedPicture) => {
          console.log('photo RESPONSES')
          console.log(savedPicture)
          ref.update({
            // email: email,
            fullname: firstName + ' ' + lastName,
            // streetAddress: newStreetAddress,
            city: city,
            state: state,
            zipcode: zipcode,
            phonenumber: phoneNumber,
            ssn: ssn,
            bankName: bankName,
            accountHolderName: accountHolderName,
            abaNumber: abaNumber,
            accountNumber: accountNumber,
            drivingLicense: drivingLicense,
            expirationDate: expirationDate,
            licensePlateNumber: licensePlateNumber,
            carMake: carMake,
            carBrand: carBrand,
            carYear: carYear,
            carColor: carColor,
            insuranceName: insuranceName,
            policyNumber: policyNumber,
            tcpNumber: tcpNumber,
            driveForUber: driveForUber,
            driveForLyft: driveForLyft,
            driveForLimo: driveForLimo,
            ownLimo: ownLimo,
            carPicture: savedPicture.downloadURL
          });
        })
        .then(() => {
          storageRef.child(regUser.uid).child('carRegistration.png')
            .putString(carRegPhotoRef, 'base64', { contentType: 'image/png' })
            .then((savedPicture) => {
              console.log('photo RESPONSES')
              console.log(savedPicture)
              ref.update({
                carRegistrationPicture: savedPicture.downloadURL
              });
            }).catch(err => { console.log(err) });
        })
        .then(() => {
          storageRef.child(regUser.uid).child('dmv.png')
            .putString(dmvPhotoRef, 'base64', { contentType: 'image/png' })
            .then((savedPicture) => {
              console.log('photo RESPONSES')
              console.log(savedPicture)
              ref.update({
                dmvPicture: savedPicture.downloadURL
              });
            }).catch(err => { console.log(err) });
        })
        .then(() => {
          storageRef.child(regUser.uid).child('insuranceCard.png')
            .putString(insuranceCardPhoto, 'base64', { contentType: 'image/png' })
            .then((savedPicture) => {
              console.log('photo RESPONSES')
              console.log(savedPicture)
              ref.update({
                insuranceCardPhoto: savedPicture.downloadURL
              });
            }).catch(err => { console.log(err) });
        }).catch(err => { console.log(err) });

      resolve(true);
    }).catch(err => console.log); // ending of new Promise

  }
  showNotification(data) {
    console.log('func ran');
    return this.alertCtrl.create({
      title: "Client Request",
      message: 'data',
      buttons: [
        {
          text: 'Decline',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Accept',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
  }
}
